<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Car.
 *
 * @mixin \Eloquent
 *
 * @property int $id
 * @property int $city_id
 * @property int $currency_id
 * @property int $company_id
 * @property string $title
 * @property string $slug
 * @property string|null $description
 * @property int $year
 * @property float $price
 * @property int $is_active
 * @property-read \App\Models\City $city
 * @property-read \App\Models\Company $company
 * @property-read \App\Models\Currency $currency
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereYear($value)
 */
class Car extends Model
{
    /**
     * @var array
     */
    protected $with = ['city', 'company', 'currency'];

    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'city_id',
        'currency_id',
        'company_id',
        'description',
        'year',
        'price',
        'is_active',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }
}
