<?php

namespace App\Http\Controllers;

use App\Http\Resources\LanguageCollection;
use App\Http\Resources\LanguageResource;
use App\Models\Language;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return LanguageCollection
     */
    public function index():LanguageCollection
    {
        $languages = Language::all();

        return new LanguageCollection($languages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $language = Language::create($request->all());

        return (new LanguageResource($language))->response()->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param Language $language
     *
     * @return LanguageResource
     */
    public function show(Language $language):LanguageResource
    {
        return new LanguageResource($language);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Language                 $language
     *
     * @return LanguageResource
     */
    public function update(Request $request, Language $language):LanguageResource
    {
        $language->update($request->all());

        return new LanguageResource($language);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Language $language
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        $language->delete();

        return response()->json('', 204);
    }
}
