<?php

namespace App\Http\Controllers;

use App\Http\Resources\CarCollection;
use App\Http\Resources\CarResource;
use App\Models\Car;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CarCollection
     */
    public function index()
    {
        $cars = Car::all();

        return new CarCollection($cars);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return CarResource
     */
    public function store(Request $request)
    {
        $car = Car::create($request->all());

        return new CarResource($car, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Car $car
     *
     * @return CarResource
     */
    public function show(Car $car)
    {
        return new CarResource($car);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Car                      $car
     *
     * @return CarResource
     */
    public function update(Request $request, Car $car)
    {
        $car->update($request->all());

        return new CarResource($car);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Car $car
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $car->delete();

        return response()->json('', 204);
    }
}
