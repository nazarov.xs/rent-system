<?php

namespace App\Http\Controllers;

use App\Http\Resources\CityCollection;
use App\Http\Resources\CityResource;
use App\Models\City;
use Illuminate\Http\Request;

/**
 * Class CityController.
 */
class CityController extends Controller
{
    /**
     * Display a listing of the city.
     *
     * @return CityCollection
     */
    public function index():CityCollection
    {
        $cities = City::all();

        return new CityCollection($cities);
    }

    /**
     * Store a newly created city in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $city = City::create($request->all());

        return (new CityResource($city))->response()->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param City $city
     *
     * @return CityResource
     */
    public function show(City $city):CityResource
    {
        return new CityResource($city);
    }

    /**
     * Update the specified city in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param City                     $city
     *
     * @return CityResource
     */
    public function update(Request $request, City $city):CityResource
    {
        $city->update($request->all());

        return new CityResource($city);
    }

    /**
     * Remove the specified city from storage.
     *
     * @param City $city
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        $city->delete();

        return response()->json('', 204);
    }
}
