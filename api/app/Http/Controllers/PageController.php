<?php

namespace App\Http\Controllers;

use App\Http\Resources\PageCollection;
use App\Http\Resources\PageResource;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the pages.
     *
     * @return PageCollection
     */
    public function index():PageCollection
    {
        $pages = Page::all();

        return new PageCollection($pages);
    }

    /**
     * Store a newly created page in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $page = Page::create($request->all());

        return (new PageResource($page))->response()->setStatusCode(201);
    }

    /**
     * Display the specified page.
     *
     * @param Page $page
     *
     * @return PageResource
     */
    public function show(Page $page):PageResource
    {
        return new PageResource($page);
    }

    /**
     * Update the specified page in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Page                     $page
     *
     * @return PageResource
     */
    public function update(Request $request, Page $page):PageResource
    {
        $page->update($request->all());

        return new PageResource($page);
    }

    /**
     * Remove the specified page from storage.
     *
     * @param Page $page
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        $page->delete();

        return response()->json('', 204);
    }
}
