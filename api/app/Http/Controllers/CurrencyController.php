<?php

namespace App\Http\Controllers;

use App\Http\Resources\CurrencyCollection;
use App\Http\Resources\CurrencyResource;
use App\Models\Currency;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CurrencyCollection
     */
    public function index():CurrencyCollection
    {
        $currencies = Currency::all();

        return new CurrencyCollection($currencies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $currency = Currency::create($request->all());

        return (new CurrencyResource($currency))->response()->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param Currency $currency
     *
     * @return CurrencyResource
     */
    public function show(Currency $currency):CurrencyResource
    {
        return new CurrencyResource($currency);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Currency                 $currency
     *
     * @return CurrencyResource
     */
    public function update(Request $request, Currency $currency):CurrencyResource
    {
        $currency->update($request->all());

        return new CurrencyResource($currency);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Currency $currency
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        $currency->delete();

        return response()->json('', 204);
    }
}
