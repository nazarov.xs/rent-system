<?php

namespace App\Http\Controllers;

use App\Http\Resources\CompanyCollection;
use App\Http\Resources\CompanyResource;
use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return CompanyCollection
     */
    public function index():CompanyCollection
    {
        $companies = Company::all();

        return new CompanyCollection($companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $company = Company::create($request->all());

        return (new CompanyResource($company))->response()->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @param Company $company
     *
     * @return CompanyResource
     */
    public function show(Company $company):CompanyResource
    {
        return new CompanyResource($company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Company                  $company
     *
     * @return CompanyResource
     */
    public function update(Request $request, Company $company):CompanyResource
    {
        $company->update($request->all());

        return new CompanyResource($company);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Company $company
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company):\Illuminate\Http\Response
    {
        $company->delete();

        return response()->json('', 204);
    }
}
