<?php

namespace App\Http\Controllers;

use App\Http\Resources\CountryCollection;
use App\Http\Resources\CountryResource;
use App\Models\Country;
use Illuminate\Http\Request;

/**
 * Class CountryController.
 */
class CountryController extends Controller
{
    /**
     * Display a listing of the country.
     *
     * @return CountryCollection
     */
    public function index():CountryCollection
    {
        $countries = Country::all();

        return new CountryCollection($countries);
    }

    /**
     * Store a newly created country in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $country = Country::create($request->all());

        return (new CountryResource($country))->response()->setStatusCode(201);
    }

    /**
     * Display the specified country.
     *
     * @param Country $country
     *
     * @return CountryResource
     */
    public function show(Country $country):CountryResource
    {
        return new CountryResource($country);
    }

    /**
     * Update the specified country in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Country                  $country
     *
     * @return CountryResource
     */
    public function update(Request $request, Country $country):CountryResource
    {
        $country->update($request->all());

        return new CountryResource($country);
    }

    /**
     * Remove the specified country from storage.
     *
     * @param Country $country
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        $country->delete();

        return response()->json('', 204);
    }
}
