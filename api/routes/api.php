<?php

Route::resource('city', CityController::class);
Route::resource('country', CountryController::class);
Route::resource('language', LanguageController::class);
Route::resource('currency', CurrencyController::class);
Route::resource('page', PageController::class);
Route::resource('company', CompanyController::class);
Route::resource('car', CarController::class);

Route::group(['prefix' => 'auth'], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::get('me', 'AuthController@me');
});
