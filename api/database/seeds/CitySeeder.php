<?php

use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $russia = \App\Models\Country::whereCode('RU')->first();
        $thailand = \App\Models\Country::whereCode('TH')->first();
        $spain = \App\Models\Country::whereCode('ES')->first();

        $m = new \App\Models\City();
        $m->name = 'Москва';
        $m->seo_name = 'moscow';
        $m->country_id = $russia->id;
        $m->save();

        $k = new \App\Models\City();
        $k->name = 'Крым';
        $k->seo_name = 'crimea';
        $k->country_id = $russia->id;
        $k->save();

        $p = new \App\Models\City();
        $p->name = 'Пхукет';
        $p->seo_name = 'phuket';
        $p->country_id = $thailand->id;
        $p->save();

        $b = new \App\Models\City();
        $b->name = 'Барселона';
        $b->seo_name = 'barcelona';
        $b->country_id = $spain->id;
        $b->save();
    }
}
