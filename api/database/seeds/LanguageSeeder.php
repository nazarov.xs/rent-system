<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $langRu = new \App\Models\Language();
        $langRu->name = 'Русский';
        $langRu->code = 'RU';
        $langRu->save();

        $langEn = new \App\Models\Language();
        $langEn->name = 'English';
        $langEn->code = 'EN';
        $langEn->save();
    }
}
