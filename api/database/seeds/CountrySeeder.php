<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countryRu = new \App\Models\Country();
        $countryRu->name = 'Россия';
        $countryRu->seo_name = 'russia';
        $countryRu->code = 'RU';
        $countryRu->save();

        $countryES = new \App\Models\Country();
        $countryES->name = 'Испания';
        $countryES->seo_name = 'spain';
        $countryES->code = 'ES';
        $countryES->save();

        $countryTH = new \App\Models\Country();
        $countryTH->name = 'Таиланд';
        $countryTH->seo_name = 'thailand';
        $countryTH->code = 'TH';
        $countryTH->save();
    }
}
