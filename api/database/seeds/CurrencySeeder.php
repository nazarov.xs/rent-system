<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rub = new \App\Models\Currency();
        $rub->name = 'Рубль';
        $rub->code = 'RUB';
        $rub->save();

        $usd = new \App\Models\Currency();
        $usd->name = 'Доллар';
        $usd->code = 'USD';
        $usd->save();

        $thb = new \App\Models\Currency();
        $thb->name = 'Тайландский Бат';
        $thb->code = 'THB';
        $thb->save();

        $eur = new \App\Models\Currency();
        $eur->name = 'Евро';
        $eur->code = 'EUR';
        $eur->save();
    }
}
