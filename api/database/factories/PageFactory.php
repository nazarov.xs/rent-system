<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Page::class, function (Faker $faker) {
    return [
        'title' => $faker->words(3, true),
        'slug'  => str_slug($faker->words(3, true)),
        'text'  => $faker->paragraphs(3, true),
    ];
});
