<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Company::class, function (Faker $faker) {
    $companyName = $faker->company;

    return [
        'user_id'     => rand(1, 10),
        'name'        => $companyName,
        'slug'        => str_slug($companyName),
        'avatar'      => $faker->imageUrl(100, 100),
        'description' => $faker->paragraph(2),
        'address'     => $faker->address,
    ];
});
