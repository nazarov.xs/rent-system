<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Car::class, function (Faker $faker) {
    $title = $faker->words(rand(3, 5), true);

    return [
        'city_id'     => rand(1, 4),
        'currency_id' => rand(1, 4),
        'company_id'  => rand(1, 10),
        'title'       => $title,
        'slug'        => str_slug($title),
        'description' => $faker->paragraph,
        'year'        => rand(2000, 2018),
        'price'       => rand(5, 1000),
        'is_active'   => rand(0, 1),
    ];
});
