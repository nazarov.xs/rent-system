<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class CountryApiTest
 * Testing country rest api.
 */
class CountryApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all countries.
     *
     * @return void
     */
    public function testGetAllCountryItems()
    {
        $response = $this->get('/api/country');
        $response->assertStatus(200)->assertJsonFragment([
            'id'       => 1,
            'code'     => 'RU',
            'name'     => 'Россия',
            'seo_name' => 'russia',
        ]);
    }

    /**
     * Test get country by id.
     *
     * @return void
     */
    public function testGetCountryById()
    {
        $response = $this->get('/api/country/1');
        $response->assertStatus(200)
            ->assertExactJson(
                [
                    'data' => [
                        'id'       => 1,
                        'code'     => 'RU',
                        'name'     => 'Россия',
                        'seo_name' => 'russia',
                    ],
                ]
            );
    }

    /**
     * Test creating country.
     *
     * @return void
     */
    public function testCreateNewCountry()
    {
        $response = $this->post('/api/country', [
            'name'     => 'Test country',
            'seo_name' => 'test_country',
            'code'     => 'TS',
        ]);
        $response->assertStatus(201)
            ->assertExactJson(
                [
                    'data' => [
                        'id'       => 4,
                        'code'     => 'TS',
                        'name'     => 'Test country',
                        'seo_name' => 'test_country',
                    ],
                ]
            );
        $this->assertDatabaseHas('countries', [
            'seo_name' => 'test_country',
        ]);
    }

    /**
     * Test editing country.
     *
     * @return void
     */
    public function testEditCountry()
    {
        $country = \App\Models\Country::create([
            'name'     => 'Test country2',
            'seo_name' => 'test_country2',
            'code'     => 'TS2',
        ]);

        $response = $this->put('/api/country/'.$country->id, [
            'name' => 'Test country 3',
        ]);
        $response->assertStatus(200)
            ->assertExactJson(
                [
                    'data' => [
                        'id'       => $country->id,
                        'code'     => $country->code,
                        'name'     => 'Test country 3',
                        'seo_name' => $country->seo_name,
                    ],
                ]
            );
        $this->assertDatabaseHas('countries', ['name' => 'Test country 3'])
            ->assertDatabaseMissing('countries', ['name' => 'Test country 2']);
    }

    /**
     * Test removing country.
     *
     * @return void
     */
    public function testRemoveCountry()
    {
        $country = \App\Models\Country::create([
            'name'     => 'Test country5',
            'seo_name' => 'test_country5',
            'code'     => 'TS2',
        ]);
        $response = $this->delete('/api/country/'.$country->id);
        $response->assertStatus(204);
        $this->assertDatabaseMissing('countries', ['id' => $country->id]);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }
}
