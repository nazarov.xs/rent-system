<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

/**
 * Class CityApiTest
 * Testing city rest api.
 */
class CityApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test get all cities.
     *
     * @return void
     */
    public function testGetAllCityItems()
    {
        $response = $this->get('/api/city');
        $response->assertStatus(200)->assertJsonFragment([
            'id'         => 1,
            'name'       => 'Москва',
            'seo_name'   => 'moscow',
            'country_id' => '1',
        ]);
    }

    /**
     * Test get city by id.
     *
     * @return void
     */
    public function testGetCityById()
    {
        $response = $this->get('/api/city/1');
        $response->assertStatus(200)
            ->assertJson(
                [
                    'data' => [
                        'id'        => 1,
                        'name'      => 'Москва',
                        'seo_name'  => 'moscow',
                        'country_id'=> 1,
                    ],
                ]
            );
    }

    /**
     * Test creating city.
     *
     * @return void
     */
    public function testCreateNewCity()
    {
        $response = $this->post('/api/city', [
            'name'           => 'Test city',
            'seo_name'       => 'test_city',
            'country_id'     => '1',
        ]);
        $response->assertStatus(201)
            ->assertJson(
                [
                    'data' => [
                        'id'             => 5,
                        'country_id'     => '1',
                        'name'           => 'Test city',
                        'seo_name'       => 'test_city',
                    ],
                ]
            );
        $this->assertDatabaseHas('cities', [
            'seo_name' => 'test_city',
        ]);
    }

    /**
     * Test editing city.
     *
     * @return void
     */
    public function testEditCity()
    {
        $city = \App\Models\City::create([
            'name'           => 'Test city2',
            'seo_name'       => 'test_city2',
            'country_id'     => '1',
        ]);

        $response = $this->put('/api/city/'.$city->id, [
            'name' => 'Test country 3',
        ]);
        $response->assertStatus(200)
            ->assertExactJson(
                [
                    'data' => [
                        'id'             => $city->id,
                        'country_id'     => $city->country_id,
                        'name'           => 'Test country 3',
                        'seo_name'       => $city->seo_name,
                    ],
                ]
            );
        $this->assertDatabaseHas('cities', ['name' => 'Test country 3'])
            ->assertDatabaseMissing('cities', ['name' => 'Test city2']);
    }

    /**
     * Test removing city.
     *
     * @return void
     */
    public function testRemoveCity()
    {
        $city = \App\Models\City::create([
            'name'           => 'Test city5',
            'seo_name'       => 'test_city5',
            'country_id'     => 1,
        ]);
        $response = $this->delete('/api/city/'.$city->id);
        $response->assertStatus(204);
        $this->assertDatabaseMissing('cities', ['id' => $city->id]);
    }

    protected function setUp()
    {
        parent::setUp();
        $this->artisan('db:seed');
    }
}
