[![pipeline status](https://gitlab.com/nazarov.xs/rent-system/badges/master/pipeline.svg)](https://gitlab.com/nazarov.xs/rent-system/commits/master)
![StyleCI](https://gitlab.styleci.io/repos/10048205/shield?branch=master)

# Rent car System

Система управления сдачи автомобилей в аренду

- **api** - Бекенд на PHP Laravel
- **frontend** - веб часть для пользователей и менеджеров
    - **landing** - вебчасть для пользователей
    - **manager** - вебчасть для менеджеров
- **docker** - Настройка окружения Docker